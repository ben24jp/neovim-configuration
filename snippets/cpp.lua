local ls = require('luasnip')
local t = ls.text_node
local s = ls.snippet
local i = ls.insert_node
local f = ls.function_node

local ret_filename = function ()
 return vim.fn.expand('%', ':t')
end

ls.add_snippets("cpp", {
  --Snippet for c file information
  s("file_meta", {
    t("/**"),
    t({ " ", "\t" }),
    t("* @file   "),
    f(ret_filename, {}),
    t({ " ", "\t" }),
    t("* @breif  "),
    i(2, "Breif about the file"),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    i(3, "* Description about the file"),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    t("* @notes"),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    t("* Revision History:"),
    t({ " ", "\t" }),
    t("*       - "),
    i(0, os.date("%d%m%y")),
    t(" BEN : Creation date"),
    t({ " ", "\t" }),
    t("*/"),
   }),

  --Snippet for c function information
  s("fun_meta", {
    t("/**"),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    t("* @brief  "),
    i(1, "Brief about the function"),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    t("* Description about the function."),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    t("* @param   "),
    i(2, "Parameters"),
    t({ " ", "\t" }),
    t("* @return  "),
    i(3, "Return value"),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    t("* Revision History:"),
    t({ " ", "\t" }),
    t("*       - "),
    i(0, os.date("%d%m%y")),
    t(" BEN : Creation date"),
    t({ " ", "\t" }),
    t("*/"),
   })
 })
