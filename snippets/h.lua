local ls = require('luasnip')
local t = ls.text_node
local s = ls.snippet
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node

local ret_filename = function ()
 return vim.fn.expand('%', ':t')
end

ls.add_snippets("h", {
  --Snippet for h file information
  s("file_meta", {
    t("/**"),
    t({ " ", "\t" }),
    t("* @file   "),
    f(ret_filename, {}),
    t({ " ", "\t" }),
    t("* @breif  "),
    i(2, "Breif about the file"),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    i(3, "* Description about the file"),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    t("* @notes"),
    t({ " ", "\t" }),
    t("*"),
    t({ " ", "\t" }),
    t("* Revision History:"),
    t({ " ", "\t" }),
    t("*       - "),
    i(0, os.date("%d%m%y")),
    t(" BEN : Creation date"),
    t({ " ", "\t" }),
    t("*/"),
   }),

  --char function prototype
  s("proto_cfun", {
    c(1, {
      t("const "),
      t("volatile "),
      t(""),
     }),
    c(2, {
      t("unsigned "),
      t("signed "),
      t(""),
     }),
    t("char "),
    i(3, "function_name"),
    t("("),
    i(4, "parameter"),
    t(");"),
   }),

  --int function prototype
  s("proto_ifun", {
    c(1, {
      t("unsigned short "),
      t("unsigned long "),
      t("signed short "),
      t("signed long "),
      t(""),
     }),
    t("int "),
    i(2, "function_name"),
    t("("),
    i(3, "parameter"),
    t(");"),
   }),

  --float function prototype
  s("proto_ffun", {
    t("float "),
    i(1, "function_name"),
    t("("),
    i(3, "parameter"),
    t(");"),
   }),

  --double function prototype
  s("proto_dfun", {
    c(1, {
      t("long "),
      t(""),
     }),
    t("double"),
    i(2, "function_name"),
    t("("),
    i(3, "parameter"),
    t(");"),
   }),

  --void function prototype
  s("proto_vfun", {
    t("void "),
    i(1, "function_name"),
    t("("),
    i(3, "parameter"),
    t(");"),
   }),
 })
