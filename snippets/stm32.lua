local ls = require('luasnip')
local t = ls.text_node
local s = ls.snippet
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node

local ret_filename = function ()
 local filename = vim.fn.expand('%', ':t')
 return filename
end

local ret_filename_no_ext = function ()
 local filename = vim.fn.expand('%:r', ':t')
 return string.upper(filename)
end

ls.add_snippets("c", {
 --stm32 main.c file
 s("stm32_main.c", {
  --Header
  t("/**"), t({ " ", "\t" }),
  t("* @file   "), f(ret_filename, {}), t({ " ", "\t" }),
  t("* @breif  "), i(1, "Breif about the file"), t({ " ", "\t" }),
  t("*"), t({ " ", "\t" }),
  t("* "), i(2, "Description about the file"), t({ " ", "\t" }),
  t("*"), t({ " ", "\t" }),
  t("* @notes"), t({ " ", "\t" }),
  t("*"), t({ " ", "\t" }),
  t("* Revision History:"), t({ " ", "\t" }),
  t("*       - "), i(0, os.date("%d%m%y")), t(" BEN : Creation date"),t({ " ", "\t" }),
  t("*/"),
  --Includes
  t({ " ", "\t" }),
  t({ " ", "\t" }),
  t('#include "main.h"'),
  t({ " ", "\t" }),
  t('#include "sys.h"'), t({ " ", "\t" }), t({ " ", "\t" }),
  --Private prototypes
  t("void SysClk_Config(void);"), t({ " ", "\t" }), t({ " ", "\t" }),
  --Main function
  t("int main(void)"), t({ " ", "\t" }),
  t("{"), t({ " ", "\t" }),
  t(" HAL_Init();"), t({ " ", "\t" }),
  t(" SysClk_Config();"), t({ " ", "\t" }),
  t(" GPIO_Init();"), t({ " ", "\t" }), t({ " ", "\t" }),
  t(" while (1) {"), t({ " ", "\t" }), t({ " ", "\t" }),
  t(" }"), t({ " ", "\t" }),
  t("}"), t({ " ", "\t" }), t({ " ", "\t" }),
  --Private function definitions (System clock configurations)
  t("void SystemClock_Config(void) {"), t({ " ", "\t" }),
  t(" RCC_OscInitTypeDef RCC_OscInitStruct = {0};"), t({ " ", "\t" }),
  t(" RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};"), t({ " ", "\t" }), t({ " ", "\t" }),
  t(" RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;"), t({ " ", "\t" }),
  t(" RCC_OscInitStruct.HSIState = RCC_HSI_ON;"), t({ " ", "\t" }),
  t(" RCC_OscIni)Struct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;"), t({ " ", "\t" }),
  t(" RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;"), t({ " ", "\t" }),
  t(" RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;"), t({ " ", "\t" }),
  t(" RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;"), t({ " ", "\t" }), t({ " ", "\t" }),
  t(" if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {"), t({ " ", "\t" }),
  t("  Error_Handler();"), t({ " ", "\t" }),
  t(" }"), t({ " ", "\t" }), t({ " ", "\t" }),
  t(" RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;"), t({ " ", "\t" }),
  t(" RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;"), t({ " ", "\t" }),
  t(" RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;"), t({ " ", "\t" }),
  t(" RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;"), t({ " ", "\t" }),
  t(" RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;"), t({ " ", "\t" }), t({ " ", "\t" }),
  t(" if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {"), t({ " ", "\t" }),
  t("  Error_Handler();"), t({ " ", "\t" }),
  t(" }"), t({ " ", "\t" }),
  t("}"), t({ " ", "\t" }), t({ " ", "\t" }),
  --Private function definitions (Error handler)
  t("void Error_Handler(void) {"), t({ " ", "\t" }),
  t(" __disable_irq();"), t({ " ", "\t" }),
  t(" while (1) {"), t({ " ", "\t" }),
  t(" }"), t({ " ", "\t" }),
  t("}")
 }),

})

ls.add_snippets("cpp", {
 --stm32 main.h file
 s("stm32_main.h", {
  --Header
  t("/**"), t({ " ", "\t" }),
  t("* @file   "), f(ret_filename, {}), t({ " ", "\t" }),
  t("* @breif  "), i(1, "Breif about the file"), t({ " ", "\t" }),
  t("*"), t({ " ", "\t" }),
  t("* "), i(2, "Description about the file"), t({ " ", "\t" }),
  t("*"), t({ " ", "\t" }),
  t("@notes"), t({ " ", "\t" }),
  t("*"), t({ " ", "\t" }),
  t("* Revision History:"), t({ " ", "\t" }),
  t("*       - "), i(0, os.date("%d%m%y")), t(" BEN : Creation date"),t({ " ", "\t" }),
  t("*/"), t({ " ", "\t" }), t({ " ", "\t" }),
  --macros
  t("#ifndef __"), f(ret_filename_no_ext), t("_H__"), t({ " ", "\t" }),
  t("#define __"), f(ret_filename_no_ext), t("_H__"), t({ " ", "\t" }), t({ " ", "\t" }),
  t('#include "stm32f1xx_hal.h"'), t({ " ", "\t" }), t({ " ", "\t" }),
  t("#endif /* __"), f(ret_filename_no_ext), t("_H__ */")
 })
})
