local cmd = vim.cmd
cmd('let g:pandoc#filetypes#handled = ["pandoc", "markdown"]')
cmd('let g:pandoc#filetypes#pandoc_markdown = 0')
