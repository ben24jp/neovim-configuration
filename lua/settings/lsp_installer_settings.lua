require('nvim-lsp-installer').setup({
    ensure_installed = {"ccls", "sumneko_lua"},
    automatic_installation = true,
    ui = {
      icons = {
        server_installed = "✓",
        server_pending = "➜",
        server_uninstalled = "✗"
      }
    }
  })

--local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
require('lspconfig').ccls.setup({})
require('lspconfig').sumneko_lua.setup({})
require('lspconfig').clangd.setup({})
require('lspconfig').rust_analyzer.setup({})
require('lspconfig').pyright.setup({})
require('lspconfig').bashls.setup({})
require('lspconfig').awk_ls.setup({})
