local g = vim.g
g.UltiSnipsExpandTrigger='<C-tab>'
g.UltiSnipsJumpForwardTrigger='<M-a>'
g.UltiSnipsJumpBackwardTrigger='<M-z>'
