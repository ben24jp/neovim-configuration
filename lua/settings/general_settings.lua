local cmd = vim.cmd
local o = vim.o
local g = vim.g

cmd('syntax on')
cmd('set relativenumber')
o.mouse='a'
cmd('set clipboard=unnamedplus')
g.nrformats='alpha,bin,octal,hex'
o.updatetime=2000
o.timeoutlen=300
o.tabstop=4
o.softtabstop=0
o.shiftwidth=4
o.expandtab = false
o.autoindent = true
cmd('set nobackup')
cmd('set nowritebackup')
o.magic = true
o.sidescroll=0
g.guioptions='rb'
o.pumheight=20
g.mapleader=","
cmd('set cc=80')
