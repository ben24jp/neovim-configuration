require('telescope').setup {
  pickers = {
    spell_suggest = {
      theme = "cursor"
    },
    git_branches = {
      theme = "dropdown"
    },
  }
}
