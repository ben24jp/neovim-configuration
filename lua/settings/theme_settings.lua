local o = vim.o
o.termguicolors = true
o.guifont="JetBrains Nerd Font Mono:h18"
o.background = 'dark'
require('onedark').setup {
    style = 'warmer'
}
require('onedark').load()
--vim.cmd.colorscheme 'zephyr'
--require('onedark').setup {
--    style = 'deep'
--}
