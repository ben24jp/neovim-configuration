local cmd = vim.cmd
local kind_icons = {
  Text = "",
  Method = "",
  Function = "",
  Constructor = "",
  Field = "",
  Variable = "",
  Class = "ﴯ",
  Interface = "",
  Module = "",
  Property = "ﰠ",
  Unit = "",
  Value = "",
  Enum = "",
  Keyword = "",
  Snippet = "",
  Color = "",
  File = "",
  Reference = "",
  Folder = "",
  EnumMember = "",
  Constant = "",
  Struct = "",
  Event = "",
  Operator = "",
  TypeParameter = ""
}

local cmp = require("cmp")

cmp.setup({
    snippet = {
      expand = function(args)
        --vim.fn["UltiSnips#Anon"](args.body)
        require('luasnip').lsp_expand(args.body)
      end,
    },
    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
        ['<C-b>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.abort(),
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
      }),
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
        { name = 'ultisnips' },
        { name = 'buffer' },
        { name = 'path' },
        { name = 'nvim_lua' },
        { name = 'treesitter' },
      }),
    formatting = {
      format = require('lspkind').cmp_format({
        mode = "symbol_text",
        menu = ({
          buffer = "[Buffer]",
          nvim_lsp = "[LSP]",
          luasnip = "[LuaSnip]",
          ultisnips = "[UltiSnips]",
          nvim_lua = "[Lua]",
          latex_symbols = "[Latex]",
        })
      }),
    },
})

cmd("highlight! CmpItemAbbrDeprecated guibg=NONE gui=strikethrough guifg=#808080")
cmd("highlight! CmpItemAbbrMatch guibg=NONE guifg=#569CD6")
cmd("highlight! CmpItemAbbrMatchFuzzy guibg=NONE guifg=#569CD6")
cmd("highlight! CmpItemKindVariable guibg=NONE guifg=#9CDCFE")
cmd("highlight! CmpItemKindInterface guibg=NONE guifg=#9CDCFE")
cmd("highlight! CmpItemKindText guibg=NONE guifg=#9CDCFE")
cmd("highlight! CmpItemKindFunction guibg=NONE guifg=#C586C0")
cmd("highlight! CmpItemKindMethod guibg=NONE guifg=#C586C0")
cmd("highlight! CmpItemKindKeyword guibg=NONE guifg=#D4D4D4")
cmd("highlight! CmpItemKindProperty guibg=NONE guifg=#D4D4D4")
cmd("highlight! CmpItemKindUnit guibg=NONE guifg=#D4D4D4")
