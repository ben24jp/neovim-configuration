local cmd = vim.cmd
cmd [[packadd packer.nvim]]
require('packer').startup(function()
  use {'wbthomason/packer.nvim',
    opt = true
  }
  use {'tjdevries/nlua.nvim',
    event = 'VimEnter *.lua',
    ft = {'lua'}
  }
  use {'townk/vim-autoclose'}
  use {'norcalli/nvim-colorizer.lua'}
  use {'sheerun/vim-polyglot'}
  use {'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate all'
  }
  use {'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
  }
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }
  use {'neovim/nvim-lspconfig'}
  use {'williamboman/nvim-lsp-installer'}

  use {'hrsh7th/cmp-nvim-lsp'}
  use {'hrsh7th/cmp-buffer'}
  use {'hrsh7th/cmp-path'}
  use {'hrsh7th/cmp-cmdline'}
  use {'hrsh7th/nvim-cmp'}
  use { 'folke/which-key.nvim' }
  use {'SirVer/ultisnips'}
  use {'quangnguyen30192/cmp-nvim-ultisnips'}
  use {'hrsh7th/cmp-nvim-lua'}
  use {'ray-x/cmp-treesitter'}
  use {'honza/vim-snippets'}
  use { 'https://github.com/weirongxu/plantuml-previewer.vim.git' }
  use { 'https://github.com/aklt/plantuml-syntax.git' }
  use { 'https://github.com/tyru/open-browser.vim.git' }
  use {"https://git.sr.ht/~whynothugo/lsp_lines.nvim"}
  use {
      "nvim-neorg/neorg",
      requires = "nvim-lua/plenary.nvim"
  }
  use { 'folke/zen-mode.nvim' }
  use { 'lewis6991/gitsigns.nvim' }
  use { 'L3MON4D3/LuaSnip' }
  use { 'saadparwaiz1/cmp_luasnip' }
  use { 'kyazdani42/nvim-web-devicons' }
  use { 'yamatsum/nvim-nonicons' }
  use { 'onsails/lspkind.nvim' }
  use { 'marko-cerovac/material.nvim' }
  use { 'bluz71/vim-moonfly-colors' }
  use { 'navarasu/onedark.nvim' }
  use { 'lukas-reineke/indent-blankline.nvim' }
  use({
    'glepnir/zephyr-nvim',
    requires = { 'nvim-treesitter/nvim-treesitter', opt = true },
  })
end)
