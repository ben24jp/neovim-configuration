local ls = require("luasnip")

ls.config.set_config({
  history = true,
  updateenevts = "TextChanges, TextChangedI",
  enable_autosnippets = true,
  ext_opt = {
   [require("luasnip.util.types").choiceNode] = {
    active = {
     virt_text = { { "", "Error" } }
    }
   }
  }
 })

--Keymaps

vim.keymap.set({ "i", "s" }, "<C-k>", function ()
 if ls.expand_or_jumpable() then
  ls.expand_or_jump()
 end
end, { silent = true })

vim.keymap.set({ "i", "s" }, "<C-j>", function ()
 if ls.jumpable(-1) then
  ls.jump(-1)
 end
end, { silent = true })

vim.keymap.set({ "i", "s" }, "<C-l>", function ()
 if ls.choice_active() then
  ls.change_choice(1)
 end
end)

ls.snippets = {
 all = {
  ls.parser.parse_snippet("expand", "--this is what was expanded"),
 },
}
