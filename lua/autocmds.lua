local Autocmd = require('utils').Autocmd

--Latex
Autocmd("BufEnter", "*.tex", ":silent set wrap linebreak nolist spell", "Latex")
Autocmd("BufWritePost", "*.tex", ":silent !bibtex %:t:r", "Latex")
Autocmd("BufWritePost", "*.tex", ":silent !compiler %", "Latex")

--SCSS
Autocmd("BufWritePost", "*.scss, *.sass", ":silent !sass % %:r.css", "SCSS")

--STM32_Templates
Autocmd("BufNewFile", "sys.h", '0r ~/.config/nvim/templates/stm32f1xx_hal_sys_h_file.h', "stm32_templates")
Autocmd("BufNewFile", "sys.c", '0r ~/.config/nvim/templates/stm32f1xx_hal_sys_h_file.c', "stm32_templates")
Autocmd("BufNewFile", "main.h", '0r ~/.config/nvim/templates/stm32f1xx_main_h_file.h', "stm32_templates")
Autocmd("BufNewFile", "main.c", '0r ~/.config/nvim/templates/stm32f1xx_main_c_file', "stm32_templates")

--RstDoc
Autocmd("BufEnter", "*.rst", ":set spell", "rstdocSpell")
Autocmd("BufWritePost", "*.rst", ":silent !rst2pdf % %:r.pdf", "rstdocPDF")
Autocmd("BufWritePost", "*.rst", ":silent !rstdoc % %:r.odt", "rstdocODT")

--PlantUML
Autocmd("BufEnter", "*.puml", ":set spell", "PlantUMLSpell")
Autocmd("BufWritePost", "*.puml", ":PlantumlSave %:r.svg", "PlantUMLSVG")
Autocmd("BufWritePost", "*.puml", ":PlantumlSave %:r.png", "PlantUMLPNG")

--C
Autocmd("BufWritePost", "*.c", ":silent !ctags -R %", "C_file_tags")
Autocmd("BufWritePost", "*.h", ":silent !ctags -R %", "H_file_tags")
Autocmd("BufWritePost", "*.cpp", ":silent !ctags -R %", "CPP_file_tags")
