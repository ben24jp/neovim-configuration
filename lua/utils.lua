local M = {}
-- Keybind function
function M.map(mode, lhs, rhs, opts)
  local options = {noremap = true}
  if opts then options = vim.tbl_extend('force', options, opts) end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

--Auto command new
function M.Autocmd(events, patterns, command, augroup)
vim.api.nvim_create_autocmd(
 { events },
 { pattern = { patterns },
   command = command,
   group = vim.api.nvim_create_augroup(augroup, { clear = true }),
 }
)
end

return M
