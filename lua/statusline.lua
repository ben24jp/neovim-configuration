local cmd = vim.cmd

cmd 'set laststatus=2'
cmd 'set statusline='
cmd 'set statusline+=%#pmenusel#'
cmd 'set statusline+=%#statusfilename#'
cmd 'set statusline+=\\ %f'
cmd 'set statusline+=%#statuscocstatus#'
cmd 'set statusline+=\\ %{coc#status()}'
cmd 'set statusline+=%m'
cmd 'set statusline+=%='
cmd 'set statusline+=%#statusfiletype#'
cmd 'set statusline+=%y'
cmd 'set statusline+=%#statusfileencoding#'
cmd 'set statusline+=\\ %{&fileencoding?&fileencoding:&encoding}'
cmd 'set statusline+=%#statusfileformat#'
cmd 'set statusline+=\\ [%{&fileformat}\\]'
cmd 'set statusline+=%#statuspercentage#'
cmd 'set statusline+=\\ %p%%'
cmd 'set statusline+=%#statuslinecolumnnumber#'
cmd 'set statusline+=\\ %l:%c'
