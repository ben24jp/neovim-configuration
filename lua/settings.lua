--Theme
require('settings.theme_settings')

--General settings
require('settings.general_settings')

-- Spelling checking
require('settings.spell_check_settings')

-- Python
require('settings.python_settings')

-- Treesitter
require('settings.treesitter_settings')

--Pandoc
require('settings.pandoc_settings')

--Telescope
require('settings.telescope_settings')

--Lualine
require('settings.lualine_settings')

--nvim-cmp
require('settings.nvim_cmp_settings')

--lsp-installer
require('settings.lsp_installer_settings')

-------------------
--  UltiSnips    --
-------------------
require('settings.ultisnip_settings')

--Which-Key
require('settings.which-key_settings')

--LSP Lines
require('settings.lsp_line_settings')

--neorg
require('settings.neorg_settings')

--zenmode
require('settings.zen-mode_settings')

--Gitsigns
require('settings.gitsigns_settings')

--luasnip
require('settings.luasnip_settings')

--Material colorscheme
require('settings.material_theme')
