local map = require('utils').map
local cmd = vim.cmd
local g = vim.g
local util = require('lspconfig').util

local lsp = require'lspconfig'
lsp.pyright.setup{on_attach=require'completion'.on_attach}
lsp.rls.setup{on_attach=require'completion'.on_attach}
lsp.rust_analyzer.setup{on_attach=require'completion'.on_attach}
lsp.ccls.setup{
  on_attach = require'completion'.on_attach,
  cmd = {"ccls"},
  filetype = {"c", "cpp", "objc", "objcpp", "arduino", "ino"},
  root_dir = util.root_pattern(".ccls", ".ccls-cache", "compile-command.json", ".git/", ".hg/", ".clang_complete")
}

map('n', '<space>,', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>')
map('n', '<space>;', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>')
map('n', '<space>a', '<cmd>lua vim.lsp.buf.code_action()<CR>')
map('n', '<space>d', '<cmd>lua vim.lsp.buf.definition()<CR>')
map('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>')
map('n', '<space>h', '<cmd>lua vim.lsp.buf.hover()<CR>')
map('n', '<space>m', '<cmd>lua vim.lsp.buf.rename()<CR>')
map('n', '<space>r', '<cmd>lua vim.lsp.buf.references()<CR>')
map('n', '<space>s', '<cmd>lua vim.lsp.buf.document_symbol()<CR>')

cmd 'set completeopt=menuone,noinsert,noselect'
g.completion_matching_strategy_list = {"fuzzy"}
g.completion_trigger_character = {".", "::"}
g.completion_enable_snippet = 'UltiSnips'
g.completion_enable_auto_hover = 1
