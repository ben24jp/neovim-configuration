local map = require('utils').map

map('', '<C-s>', ':w<CR>', {silent = true})
map('', '<C-x>', ':q!<CR>', {silent = true})
map('', '<C-M-x>', ':wqa!<CR>', {silent = true})
map('', '<C-t>', ':tabnew<CR>', {silent = true})
map('', '<M-t>', ':tabnew<CR>:terminal<CR>i<CR>', {silent = true})
map('n', '<M-s>', 'z=', {})
map('n', '<C-M-w>', ':mksession ~/.config/nvim/sessions/', {silent = true})
map('n', '<C-M-o>', ':source ~/.config/nvim/sessions/', {silent = true})
map('', '<C-Right>', 'gt', {silent = true})
map('', '<C-Left>', 'gT', {silent = true})

map('n', '<leader>ff', ':lua require("telescope.builtin").find_files()<CR>', {silent = true})
map('n', '<leader>ss', ':lua require("telescope.builtin").spell_suggest()<CR>', {silent = true})
map('n', '<leader>gb', ':lua require("telescope.builtin").git_branches()<CR>', {silent = true})
map('n', '<leader>gs', ':lua require("telescope.builtin").git_status()<CR>', {silent = true})
map('n', '<leader>fh', ':lua require("telescope.builtin").help_tags()<CR>', {silent = true})
map('n', '<leader>ft', ':lua require("telescope.builtin").treesitter()<CR>', {silent = true})
map('n', '<leader>gc', ':lua require("telescope.builtin").git_commits()<CR>', {silent = true})
map('n', '<leader>fb', ':lua require("telescope.builtin").buffers()<CR>', {silent = true})
map('n', '<leader>fct', ':lua require("telescope.builtin").tags()<CR>', {silent = true})
map('n', '<leader>fm', ':lua require("telescope.builtin").man_pages()<CR>', {silent = true})

map('n', '<leader>l', ':lua require("lsp_lines").toggle()<CR>', {silent = true})

map('n', '<space>ca', ':lua vim.lsp.buf.code_action()<CR>', {silent = true})

map('n', '<leader><space>cs', ':source ~/.config/nvim/init.lua <CR>', {silent = true})

map('n', '<leader><space>t', ':lua require("telescope.builtin").colorscheme()<CR>', { silent = true })
