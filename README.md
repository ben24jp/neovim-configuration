#Neovim configuration

This is the neovim configuration that I use to daily drive for my workflow.

## Installation

1. Install the latest nightly version of neovim.
   Download and install [Neovim Nightly](https://github.com/neovim/neovim/releases)

2. Install package manager [Packer](https://github.com/wbthomason/packer.nvim.git)

3. clone this repo to your neovim configuration folder.
   `git clone https://gitlab.com/ben24jp/neovim-configuration.git ~/.config/nvim`

4. launch neovim, you may see so many errors ignore all of that and press enter.
   type the following command in neovim.
   `:PackerInstall`
   The packages will get install after that.

5. You are all set now.

*If you are using kitty terminal add the following to your kitty configuration. This will help in rendering correct icon font*

```
symbol_map U+f101-U+f208 nonicons
symbol_map U+ea60-U+ec02 codicons
```

